FROM golang:1.15 as build
WORKDIR /go/src/app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
ENV CGO_ENABLED=0
RUN go build -o webapp

FROM scratch as final
COPY --from=build /go/src/app/webapp /bin/webapp
ENTRYPOINT ["/bin/webapp"]

EXPOSE 5000